var PropertyView = Backbone.View.extend({
	tagName: 'tr',

	template: _.template($('#propertyInfo').html()),

	events: {
		"dblclick span": "toggleEdit",
		"keyup input": "update",
		"click td:last": "delete"
	},

	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},

	toggleEdit: function() {
		elements = this.$el.find('.hide');
		elements.siblings().addClass('hide');
		elements.removeClass('hide');
	},

	update: function(e){
		e.preventDefault();
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if(keycode == 13) {
			var raw_data = this.$el.find('input').serializeArray();		//Fetches data from all the input tags
			var attributes = this.serializeFormDataToObject(raw_data);
			this.model.set(attributes);
		}
		else if(keycode == 27) {
			this.toggleEdit();
		}
	},

	delete: function() {

	},

	serializeFormDataToObject: function(raw_data){
		/*
		Serializes the raw data to an object
		Example:
			[{name: 'city', value:'New York'}]  =>  [['city', 'New York']]  =>  {city: 'New York'}
		*/
		var list = [];
		_.each(raw_data, function(obj){list.push(_.values(obj))});
		return _.object(list);
	}
});