$.ajax({
	url: 'http://localhost:8000/api/v1/property/?format=json&limit=10',
	type: 'GET',
	dataType: 'json',
}).done(function(data){
	properties = new Properties(data.objects);
	var propertiesView = new PropertiesView({collection: properties});
	$('#properties-container').append(propertiesView.render().el);
});
