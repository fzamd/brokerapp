var PropertiesView = Backbone.View.extend({
	tagName: 'tbody',

	render: function() {
		this.collection.each(function(property) {
			var propertyView = new PropertyView({model: property});
			this.$el.append(propertyView.render().el);
		}, this);
		return this;
	}
});