var Property = Backbone.Model.extend({
	defaults: {
		mls_id: '',
		start_time: '',
		end_time: '',
		address: 'Area-51',
		city: 'Nevada',
		beds: 5,
		baths: 5,
		price: '$5000',
		days_on_market: '2',
		property_type: '',
		listing_agent: {}
	}
});